# Trang Do & Mohammad Aga
# Diplomacy
# TestDiplomacy.py
# Created: 10/21/2022
# Last Modified: 10/21/2022

# Requires Diplomacy.py

import sys
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

class MyUnitTests (TestCase):

    # corner case: empty input
    def test_solve_1(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")
    
    # corner case: white space input
    def test_solve_2(self):
        r = StringIO(" ")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    # corner case: 1 army
    def test_solve_3(self):
        r = StringIO("A Madrid Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\n")

    # complex case: different numbers of supporters with some nullified
    def test_solve_4(self):
        r = StringIO(
            "A Madrid Move Barcelona\n" \
            "B Barcelona Hold\n" \
            "C London Move Barcelona\n" \
            "D Paris Support A\n" \
            "E Austin Support C\n" \
            "F Dallas Support C\n" \
            "G Houston Move Dallas\n" \
            "H NewYork Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB [dead]\nC [dead]\nD Paris\nE Austin\nF [dead]\nG [dead]\nH NewYork\n")

if __name__ == "__main__": #pragma: no cover
    main()
